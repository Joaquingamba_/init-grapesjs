import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectComponent } from './select/select.component';
import { GrapesComponent } from './grapes/grapes.component';

const routes: Routes = [
  { path: 'select', component: SelectComponent },
  { path: 'editor', component: GrapesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
