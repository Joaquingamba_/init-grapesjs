import { Injectable } from '@angular/core';
@Injectable()
export class GrapesComponentService {
  private components = [];

  constructor() {
    this.onInit();
  }

  onInit(): void {
    this.components.push({
      tagName: 'div',
      type: 'head-container',
      name: 'head-container',
      draggable: false,
      removable: false,
      attributes: {
        class: 'template-head-container',
        style: 'min-height: 100px; background-color: #5555;'
      },
      components: []
    }, {
      tagName: 'div',
      type: 'body-container',
      name: 'body-container',
      draggable: false,
      removable: false,
      attributes: {
        style: 'min-height: 250px;'
      },
      classes: [{ name: 'template-body-container' }, { name: 'lala' }],
      components: []
    }, {
      tagName: 'div',
      type: 'footer-container',
      name: 'footer-container',
      draggable: false,
      removable: false,
      attributes: {
        class: 'template-footer-container',
        style: 'min-height: 100px; background-color: #5555;'
      },
      components: []
    });
  }

  getComponents(): any {
    return this.components;
  }
}