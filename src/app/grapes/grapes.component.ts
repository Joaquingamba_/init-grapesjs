import { Component, OnInit, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import grapesjs from 'grapesjs';
import { GrapesComponentService } from './components/grapes-components.service';
import { GrapesRTEComponentService } from './services/grapes-rte.serve';

@Component({
  selector: 'app-grapes',
  templateUrl: './grapes.component.html',
  styleUrls: ['./grapes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class GrapesComponent implements OnInit {
  private editor: any;

  constructor(
    private componentsManager: GrapesComponentService,
    private customRTE: GrapesRTEComponentService
  ) { }

  ngOnInit() {
    this.editor = grapesjs.init({
      // Indicate where to init the editor. You can also pass an HTMLElement
      container: '#gjs',
      // Get the content for the canvas directly from the element
      // As an alternative we could use: `components: '<h1>Hello World Component!</h1>'`,
      fromElement: false,
      // Size of the editor
      height: '500px',
      width: 'auto',
      // Disable the storage manager for the moment
      storageManager: false,
      // Avoid any default panel
      panels: {},

      blockManager: {
        appendTo: '#blocks',
        blocks: [
          {
            id: 'section', // id is mandatory
            label: '<b>Section</b>', // You can use HTML/SVG inside labels
            attributes: { class: 'gjs-block-section' },
            content: `<section>
                        <h1>This is a simple title</h1>
                        <div>This is just a Lorem text: Lorem ipsum dolor sit amet</div>
                      </section>`,
          }, {
            id: 'text',
            label: 'Text',
            attributes: { class: 'gjs-block-section' },
            content: '<div data-gjs-type="text"><input name="lala" value="lala" />Insert your text here</div>',
          }, {
            id: 'image',
            label: 'Image',
            attributes: { class: 'gjs-block-section' },
            // Select the component once it's dropped
            select: true,
            // You can pass components as a JSON instead of a simple HTML string,
            // in this case we also use a defined component type `image`
            content: { type: 'image' },
            // This triggers `active` event on dropped components and the `image`
            // reacts by opening the AssetManager
            activate: true,
          }
        ]
      },
      styleManager: {
        appendTo: '#gjs',
        sectors: [{
          open: false
        }]
    }



    });
    this.editor.DomComponents.addType('input', {
      isComponent: el => el.tagName == 'INPUT',
      model: {
        defaults: {
          traits: [
            // Strings are automatically converted to text types
            'name', // Same as: { type: 'text', name: 'name' }
            'placeholder',
            {
              type: 'select', // Type of the trait
              label: 'Type', // The label you will see in Settings
              name: 'type', // The name of the attribute/property to use on component
              options: [
                { id: 'text', name: 'Text' },
                { id: 'email', name: 'Email' },
                { id: 'password', name: 'Password' },
                { id: 'number', name: 'Number' },
              ]
            }, {
              type: 'checkbox',
              name: 'required',
            }],
          // As by default, traits are binded to attributes, so to define
          // their initial value we can use attributes
          attributes: { type: 'text', required: true },
        },
      },
    });
    this.customRTE.customize(this.editor);

    this.editor.Canvas.getBody().droppable = false;
    this.editor.setStyle({
      selectors: ['template-body-container'],
      style: { 'background-color': '#000000' }
    });

    const components = this.componentsManager.getComponents().map(component => this.editor.addComponents(component));

    const domComponents = this.editor.DomComponents;

    const wrapper = domComponents.getWrapper();
    wrapper.set({droppable: false});
    // wrapper.set('style', {'margin-top': '40px'});
    console.log('wrapper', wrapper);


  }
}
