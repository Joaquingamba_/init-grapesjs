import { Injectable } from '@angular/core';

@Injectable()
export class GrapesRTEComponentService {
  private json: Array<any> = [];
  private options: Array<string> = [];

  constructor() {
    this.buildJson();
    this.buildOptions();
  }

  private buildJson(): void {
    this.json = [
      {
        name: 'firstname',
        value: 'user.firstname',
      },
      {
        name: 'lastname',
        value: 'user.lastname',
      },
      {
        name: 'age',
        value: 'user.age',
      },
    ];
  }

  private buildOptions(): void {
    this.options = this.json.map(placeholder => {
      let value = `<span value='${placeholder.value}' style='background-color: yellow;'>{${placeholder.name}}</span>`;
      return `<option value="${value}">${placeholder.name}</option>`;
    })
  }

  public customize(editor: any): void {
    let options = '';
    this.options.forEach(option => options = `${options}${option}`);
    editor.RichTextEditor.add('custom-vars', {
      icon: `<select class="gjs-field">${options}</select>`,
      // Bind the 'result' on 'change' listener
      event: 'change',
      result: (rte, action) => rte.insertHTML(action.btn.firstChild.value),
      // Reset the select on change
      update: (rte, action) => { action.btn.firstChild.value = ""; }
    })
  }
}