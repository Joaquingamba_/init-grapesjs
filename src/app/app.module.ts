import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GrapesComponent } from './grapes/grapes.component';
import { GrapesComponentService } from './grapes/components/grapes-components.service';
import { CkeditorComponent } from './ckeditor/ckeditor.component';
import { GrapesRTEComponentService } from './grapes/services/grapes-rte.serve';

import { SelectComponent } from './select/select.component';

import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    AppComponent,
    GrapesComponent,
    CkeditorComponent,
    SelectComponent
  ],
  imports: [
    CKEditorModule,
    BrowserModule,
    ModalModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    GrapesComponentService,
    GrapesRTEComponentService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
